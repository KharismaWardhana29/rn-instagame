import Icons from 'react-native-vector-icons/MaterialIcons';
import React from 'react';

const tabBarIcon = name => ({ tintColor }) => (
  <Icons
    style={{ backgroundColor: 'transparent' }}
    name={name}
    color={tintColor}
    size={24}
  />
);

export default tabBarIcon;
