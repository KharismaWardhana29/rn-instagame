import * as ImageManipulator from 'expo-image-manipulator';

function reduceImageAsync(uri) {
  // const cropData = {
  //   offset: {x: 0, y: 0},
  //   size: {width: 500, height: 500},
  //   displaySize: {width: 500, height: 500},
  //   resizeMode: 'contain/cover/stretch',
  // };

  // let croppedImageURI = null;
  // let cropError = null;

  // ImageEditor.cropImage(
  //   uri,
  //   cropData,
  //   croppedImageURI => croppedImageURI,
  //   cropError => this.setState({cropError}),
  // );
  return ImageManipulator.manipulateAsync(uri, [{ resize: { width: 500 } }], {
    compress: 0.5,
  });
}
export default reduceImageAsync;
