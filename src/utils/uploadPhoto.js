import firebase from 'react-native-firebase';
function uploadPhoto(uri, uploadUri) {
  return new Promise(async (res, rej) => {
    // const response = await fetch(uri);
    // const blob = await response.blob();

	const instaGameApp = firebase.app('instagameloft');

    const ref = firebase.storage(instaGameApp).ref(uploadUri);
    const unsubscribe = ref.putFile(uri).on(
      'state_changed',
      state => {},
      err => {
        unsubscribe();
        rej(err);
      },
      async () => {
        unsubscribe();
        const url = await ref.getDownloadURL()
							.then((url) => {
								console.log(url);
								res(url);
							}).catch(console.log('error'));
        
      },
    );
  });
}

export default uploadPhoto;
