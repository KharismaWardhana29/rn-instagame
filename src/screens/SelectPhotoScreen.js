import Constants from 'expo-constants';
// import * as ImagePicker from 'expo-image-picker';
import ImagePicker from 'react-native-image-picker';
import * as Permissions from 'expo-permissions';

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

import getPermission from '../utils/getPermission';

const options = {
  allowsEditing: true,
};

export default class SelectPhotoScreen extends Component {
  state = {};

  _selectPhoto = async () => {
    try
    {
      const status = await getPermission(Permissions.CAMERA_ROLL);
      if (status) {
        const result = await ImagePicker.launchImageLibrary(options, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else
          {
            this.props.navigation.navigate('NewPost', { image: response.uri });
          }

        });
      }
    }
    catch ({ message }) { console.log(message) };
  };

  _takePhoto = async () => {
    try
    {
      const status = await getPermission(Permissions.CAMERA);
      if (status) {
        const result = await ImagePicker.launchCamera(options, (response) => {
          console.log('Response = ', response);
        
          if (response.didCancel) {
            console.log('User cancelled image picker');
          }
          else
          {
            this.props.navigation.navigate('NewPost', { image: response.uri });
          }
          
        });
      }
    }
    catch ({ message }) { console.log(message) };
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableHighlight onPress={this._selectPhoto}>
          <Text style={styles.text}>Select Photo</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={this._takePhoto}>
          <Text style={styles.text}>Take Photo</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    padding: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
