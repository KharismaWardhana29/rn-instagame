import { Platform } from 'react-native';
import uuid from 'uuid';
import getUserInfo from '../utils/getUserInfo';
import shrinkImageAsync from '../utils/shrinkImageAsync';
import uploadPhoto from '../utils/uploadPhoto';
import firebase from 'react-native-firebase';

const collectionName = 'snack-SJucFknGX';

class Fire {

  constructor() {
    const config = {
      clientId: '208381195874-16crkd3ci9gt8d7ioopr7d0md8im73dd.apps.googleusercontent.com',
      appId: 'instagame-rn-gameloft.firebaseapp.com',
      apiKey: 'AIzaSyBR3pkgdBZ_j0OIbSCdb3ueEswAZ0wkj1E',
      databaseURL: 'https://instagame-rn-gameloft.firebaseio.com',
      storageBucket: 'instagame-rn-gameloft.appspot.com',
      messagingSenderId: '208381195874',
      projectId: 'instagame-rn-gameloft', 
    };

    firebase.initializeApp( config, 'instagameloft' );

    // Listen for auth
    firebase.app('instagameloft').auth().onAuthStateChanged(async user => {
      if (!user) {
        await firebase.app('instagameloft').auth().signInAnonymously();
      }
    });
  }

  // Download Data
  getPaged = async ({ size, start }) => {
    let ref = this.collection.orderBy('timestamp', 'desc').limit(size);
    try {
      if (start) {
        ref = ref.startAfter(start);
      }

      const querySnapshot = await ref.get();
      const data = [];
      querySnapshot.forEach(function(doc) {
        if (doc.exists) {
          const post = doc.data() || {};

          // Reduce the name
          const user = post.user || {};

          const name = user.deviceName;
          const reduced = {
            key: doc.id,
            name: (name || 'Secret Duck').trim(),
            ...post,
          };
          data.push(reduced);
        }
      });

      const lastVisible = querySnapshot.docs[querySnapshot.docs.length - 1];
      return { data, cursor: lastVisible };
    } catch ({ message }) {
      alert(message);
    }
  };

  // Upload Data
  uploadPhotoAsync = async uri => {
    const path = `${collectionName}/${this.uid}/${uuid.v4()}.jpg`;
	console.log(uri);
    return uploadPhoto(uri, path);
  };

  post = async ({ text, image: localUri }) => {
    try {
      // const { uri: reducedImage, width, height } = await shrinkImageAsync(
      //   localUri,
      // );

      const remoteUri = await this.uploadPhotoAsync(localUri);
      this.collection.add({
        text,
        uid: this.uid,
        timestamp: this.timestamp,
        imageWidth: 500,
        image: remoteUri,
        user: getUserInfo(),
      });
    } catch ({ message }) {
      alert(message);
    }
  };

  // Helpers
  get collection() {
    return firebase.app('instagameloft').firestore().collection(collectionName);
  }

  get uid() {
    return (firebase.app('instagameloft').auth().currentUser || {}).uid;
  }
  get timestamp() {
    return Date.now();
  }
}

const instaGameloft = new Fire();
export default instaGameloft;
